import urlparse
from threading import Timer
from pywebcrawler.crawler import WebCrawler, Journal
from pywebcrawler.parser import Parser


DOMAIN = '9gag.com'


class J(Journal):
    def may_enter(self, url):
        p = urlparse.urlparse(url)
        if p.netloc == DOMAIN:
            return super(J, self).may_enter(url);
        return False


def print_status():
    crawler.status()
    t = Timer(5.0, print_status)
    t.start()


journal = J()
journal.add_url('http://%s/' % DOMAIN)

parser = Parser()

try:
    print "STARTER"
    crawler = WebCrawler(journal, parser)
    crawler.start(10)
    print_status()
    while crawler.running():
        pass
except KeyboardInterrupt:
    print "STOPPER"
    crawler.stop()
