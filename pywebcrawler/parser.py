from bs4 import BeautifulSoup


class Parser(object):
    def __call__(self, *args, **kwargs):
        return self.fetch_urls(*args, **kwargs)
    
    def fetch_urls(self, data):
        soup = BeautifulSoup(data)
        anchors = soup.findAll('a')
        urls = set()
        for anchor in anchors:
            href = anchor.get('href')
            if href is not None:
                urls.add(href)
        return urls

    def parse(self, request, response, data):
        pass
