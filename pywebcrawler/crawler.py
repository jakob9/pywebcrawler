import threading
import urllib2
import urlparse
import socket
from Queue import Queue


HTTP_TIMEOUT = 10


class Journal(object):
    def __init__(self):
        self._url_queue = Queue()
        self._known_urls = set()
        self._mutex = threading.Lock()

    def add_url(self, url):
        """
        Add a single URL to the queue.

        Args:
            (str) url: URL to be added to the queue and crawled later on.
        """
        self.add_urls([url])

    def add_urls(self, urls):
        """
        Add multiple URLs to the queue. This method is more effective than
        doing multiple calls to add_url() since it only acquires and releases
        the mutex once, and therefore has less overhead.

        Args:
            (list) urls: An iterable object of URLs (strings) to be added
                to the queue and crawled later on.
        """
        for url in urls:
            self._url_queue.put(url, block=True)
        self._mutex.acquire()
        for url in urls:
            self._known_urls.add(url)
        self._mutex.release()

    def next_url(self):
        """
        Get the next URL to be crawled.

        Returns:
            (str) The next URL to be crawled.
        """
        return self._url_queue.get(block=True)

    def is_known(self, url):
        """
        Check whether a URL is already known.

        Returns:
            (bool) Whether or not a URL is already known.
        """
        return (url in self._known_urls)

    def may_enter(self, url):
        """
        Decides whether or not a URL may be crawled.

        Returns:
            (bool) Whether or not a URL may be crawled.
        """
        return not self.is_known(url)

    def build_request(self, url):
        """
        Build a urllib2 Request-object from a URL.

        Returns:
            (urllib2.Request) Request object.
        """
        return urllib2.Request(url=url)

    def on_socket_error(self, request, reason):
        """

        """
        pass

    def on_http_error(self, request, code, reason):
        """

        """
        pass


class _Spider(threading.Thread):
    def __init__(self, i, journal, parser, *args, **kwargs):
        super(_Spider, self).__init__(*args, **kwargs)
        self._journal = journal
        self._parser = parser
        self._alive = False
        self.i = i
        self.count = 0
        self.lcount = 0
        self.ecount = 0

    def run(self):
        self._alive = True

        while self._alive:
            request_url = self._journal.next_url()
            request = self._journal.build_request(request_url)

            self.count += 1

            # Fetch data at URL and invoke error handling if required
            try:
                response = urllib2.urlopen(request, timeout=HTTP_TIMEOUT)
            except urllib2.URLError, e:
                self._journal.on_socket_error(request, e.reason)
                continue
            except urllib2.HTTPError, e:
                self._journal.on_http_error(request, e.code, e.reason)
                continue
            except:
                self.ecount += 1
                print 'UNKNOWN ERROR IN #%d' % (self.i+1)
                continue

            # Handle timeout on reading data
            try:
                data = response.read()
            except socket.timeout:
                self._journal.on_socket_error(request, 'timeout')
                continue

            # Fetch URLs from response HTML via Parser
            urls = set()
            for url in self._parser(data):
                url = urlparse.urljoin(request_url, urlparse.urldefrag(url)[0])
                url_parsed = urlparse.urlparse(url)

                if not url_parsed.scheme in ('http', 'https'):
                    continue

                if self._journal.may_enter(url):
                    urls.add(url)

            # Add fetched URLs to Journal
            self._journal.add_urls(urls)

            # Invoke Parser to do additional parsing of the Response
            if hasattr(self._parser, 'parse'):
                self._parser.parse(request, response, data)

    def stop(self):
        """
        Request the Spider to stop working.
        """
        self._alive = False

    def status(self):
        delta = self.count - self.lcount
        s = 'STOPPET?' if not delta else ''
        print "#%-2d = %-2d (+%2d) (FEJL: %-2d)  %s" % (self.i+1, self.count, delta, self.ecount, s)
        self.lcount = self.count


class WebCrawler(object):
    def __init__(self, journal, parser):
        self._journal = journal
        self._parser = parser
        self._spiders = []
        self.t = 0
        self.l = 0
    
    def start(self, workers):
        """
        Initiates worker threads and starts crawling.

        Args:
            (int) workers: The amount of asynchronous workers (threads).
        """
        for i in xrange(workers):
            spider = _Spider(i, self._journal, self._parser)
            spider.start()
            self._spiders.append(spider)
    
    def stop(self, join=True):
        """
        Stop currently running workers.

        Args:
            (bool) join: Whether or not to join threads
                after requesting them to stop.
        """
        for spider in self._spiders:
            spider.stop()
        if join:
            self.join()

    def running(self):
        """
        Check whether workers are working.
        """
        for spider in self._spiders:
            if spider.isAlive():
                return True

        return False

    def join(self):
        """
        Join all running threads.
        """
        for spider in self._spiders:
            spider.join()

    def status(self):
        self.t += 1
        total = 0
        for spider in self._spiders:
            spider.status()
            total += spider.count
        print "TIME:  %d sec" % (self.t * 5)
        print "TOTAL: %d pages (+ %d)" % (total, total-self.l)
        print "AVG:   %f pages/sec" % (float(total) / (self.t * 5))
        print "QUEUE: %d" % self._journal._url_queue.qsize()
        print '-' * 25
        self.l = total

